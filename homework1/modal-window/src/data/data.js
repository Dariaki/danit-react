export const modals = [
    {
        id: 'modal1',
        header: 'Do you want to delete this file?',
        text: 'Once you delete this file, it won\'t be possible to undo this action. Are you sure you want to delete it?',
        closeButton: true
    },
    {
        id: 'modal2',
        header: 'Save file',
        text: 'Once you save this file, it will be downloaded to your computer at the local directory',
        closeButton: false,
    }
];

export const btnActions = [
    {
        id: 'btnAction1',
        backgroundColor: 'rgb(236, 236, 236)',
        text: 'Ok',
    },
    {
        id: 'btnAction2',
        backgroundColor: 'rgb(236, 236, 236)',
        text: 'Cancel',
    }
];


export const btnProperties = [
    {
        id: 'btn1',
        backgroundColor: 'rgb(225, 138, 138)',
        text: 'Open first modal',
    },
    {
        id: 'btn2',
        backgroundColor: 'rgb(83, 207, 225)',
        text: 'Open second modal',
    }
];
