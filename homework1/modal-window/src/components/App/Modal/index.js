import React, {Component} from 'react';
import ModalButton from './ModalButton.js'
import './modal.scss'
import PropTypes from "prop-types";


export default class Modal extends Component {

    closeModal = (event) => {

        const { closeModal } = this.props;
       if (event.target.className.includes('modal-window') || event.target.className.includes('close-button')) {
           closeModal();
       }
    };

    static propTypes = {
        button: PropTypes.object.isRequired,
        content: PropTypes.array.isRequired,
        actions:  PropTypes.array.isRequired,
    };

    render() {

        const {button, content, actions} = this.props;
        let contentModal;
        button.id === 'btn1' ? contentModal = content[0] : contentModal = content[1];

        return (
           <div className="modal-window" onClick={this.closeModal}>
               <div className="modal-content">

                   <div className="modal-header">
                       <p>{contentModal.header}</p>
                       {contentModal.closeButton ? <button className="close-button">X</button> : ''}
                   </div>
                   <div className="modal-body">
                       <p>{contentModal.text}</p>
                   </div>

                   <div className="modal-footer">
                       {actions.map(obj => <ModalButton key={obj.id} btnProps={obj}/>)}
                   </div>
               </div>
           </div>
        )
    }
}

