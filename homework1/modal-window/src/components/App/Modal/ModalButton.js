import React, {Component} from 'react';
import PropTypes from "prop-types";


export default class ModalButton extends Component {

    static propTypes = {
        btnProps: PropTypes.object.isRequired,
    };

    render() {

        const { btnProps } = this.props;
        const {
            backgroundColor,
            text
        } = btnProps;


        return (
            <div>
                <button
                    className="btn modal-button"
                    style={{backgroundColor: backgroundColor}}>
                    {text}
                </button>
            </div>
        )
    }
}

