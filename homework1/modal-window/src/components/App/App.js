import React, { Component } from 'react';

import './App.scss';
import { modals, btnActions, btnProperties } from '../../data/data.js'
import ButtonContainer from '../../containers/ButtonContainer'
import Modal from "./Modal";


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpened: false,
            buttonData: ''
        }
    }


    openModalWindow = (btnProps) => {

        this.setState({buttonData: btnProps});
        this.setState({isOpened: true})
    };

    closeModalWindow = () => {

        this.setState({isOpened: false})
    };

    render() {

        return (
          <div className="App">
            <ButtonContainer
                openModalWindow={this.openModalWindow}
                btnProperties={btnProperties}
            />

              {this.state.isOpened ? <Modal
                  button={this.state.buttonData}
                  content={modals}
                  actions = {btnActions}
                  closeModal={this.closeModalWindow}/> : ''}
          </div>
        );
    }


}


export default App;
