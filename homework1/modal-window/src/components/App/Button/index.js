import React, {Component} from 'react';
import PropTypes from "prop-types";

import './button.scss';

export default class Button extends Component {

    openModalWindow = () => {
        const { openModalWindow, btnProps } = this.props;

        openModalWindow(btnProps);
    };

    static propTypes = {
        btnProps: PropTypes.object.isRequired,
        openModalWindow: PropTypes.func.isRequired
    };


    render() {

        const { btnProps } = this.props;
        const {
            backgroundColor,
            text
        } = btnProps;

        return (
            <div>
                <button
                    onClick={this.openModalWindow}
                    className="btn"
                    style={{backgroundColor: backgroundColor}}>
                    {text}
                </button>
            </div>
        )
    }
}

