import React, {Component} from 'react';
import PropTypes from "prop-types";

import './button-container.scss';

import Button from '../../components/App/Button'

export default class ButtonContainer extends Component {


    openModalWindow = (btnProps) => {
        const { openModalWindow } = this.props;

        openModalWindow(btnProps);
    };

    static propTypes = {
        btnProperties: PropTypes.array.isRequired,
        openModalWindow: PropTypes.func.isRequired
    };


    render() {

        const { btnProperties } = this.props;


        return (
            <div className="main-container">
                {
                    btnProperties.map(obj => <Button key={obj.id} btnProps={obj} openModalWindow={this.openModalWindow}/>)
                }
            </div>
        )
    }
}

